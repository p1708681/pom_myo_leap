import de.voidplus.myo.*;
import de.voidplus.leapmotion.*;

Myo myo;
LeapMotion leap;
int previousTime;
float currentHandGrab;
int nbOutstretchedFingers;

boolean[] active;

void setup() {
  size(800, 500);
  background(255,255,255);
  currentHandGrab = 0.0;
  nbOutstretchedFingers = 0;

  myo = new Myo(this);
  leap = new LeapMotion(this).allowGestures();
  // myo.setVerbose(true);
  // myo.setVerboseLevel(2); // Default: 1 (1-3)
  
  
  myo.setFrequency(10);
  
  active = new boolean[5];
  resetImages();
}

void resetImages(){
  for(int i = 0; i<5; i++){
    active[i] = false;
  }
}

void draw() {
  // active[4] == FIST
  if(active[4] && nbOutstretchedFingers == 0 && currentHandGrab > 0.85) {
    background(255,0,0);
  } else if(active[1] && nbOutstretchedFingers == 5) { // active[1] == SPREAD
    background(0,255,0);
  } else {
    background(255*currentHandGrab,255,255);
  }
  
  
  for (Hand hand : leap.getHands()) {
    // ==================================================
    // 2. Hand

    int     handId             = hand.getId();
    PVector handPosition       = hand.getPosition();
    PVector handStabilized     = hand.getStabilizedPosition();
    PVector handDirection      = hand.getDirection();
    PVector handDynamics       = hand.getDynamics();
    float   handRoll           = hand.getRoll();
    float   handPitch          = hand.getPitch();
    float   handYaw            = hand.getYaw();
    boolean handIsLeft         = hand.isLeft();
    boolean handIsRight        = hand.isRight();
    float   handGrab           = hand.getGrabStrength();
    float   handPinch          = hand.getPinchStrength();
    float   handTime           = hand.getTimeVisible();
    PVector spherePosition     = hand.getSpherePosition();
    float   sphereRadius       = hand.getSphereRadius();

    // --------------------------------------------------
    // Drawing
    if(handIsLeft) {
      currentHandGrab = handGrab;
      nbOutstretchedFingers = hand.getOutstretchedFingers().size();      
    }
    hand.draw();


    // ==================================================
    // 3. Arm

    if (hand.hasArm()) {
      de.voidplus.leapmotion.Arm     arm              = hand.getArm();
      float   armWidth         = arm.getWidth();
      PVector armWristPos      = arm.getWristPosition();
      PVector armElbowPos      = arm.getElbowPosition();
    }


    // ==================================================
    // 4. Finger

    Finger  fingerThumb        = hand.getThumb();
    // or                        hand.getFinger("thumb");
    // or                        hand.getFinger(0);

    Finger  fingerIndex        = hand.getIndexFinger();
    // or                        hand.getFinger("index");
    // or                        hand.getFinger(1);

    Finger  fingerMiddle       = hand.getMiddleFinger();
    // or                        hand.getFinger("middle");
    // or                        hand.getFinger(2);

    Finger  fingerRing         = hand.getRingFinger();
    // or                        hand.getFinger("ring");
    // or                        hand.getFinger(3);

    Finger  fingerPink         = hand.getPinkyFinger();
    // or                        hand.getFinger("pinky");
    // or                        hand.getFinger(4);


    for (Finger finger : hand.getFingers()) {
      // or              hand.getOutstretchedFingers();
      // or              hand.getOutstretchedFingersByAngle();

      int     fingerId         = finger.getId();
      PVector fingerPosition   = finger.getPosition();
      PVector fingerStabilized = finger.getStabilizedPosition();
      PVector fingerVelocity   = finger.getVelocity();
      PVector fingerDirection  = finger.getDirection();
      float   fingerTime       = finger.getTimeVisible();

      // ------------------------------------------------
      // Drawing

      // Drawing:
       //finger.draw();  // Executes drawBones() and drawJoints()
       //finger.drawBones();
       //finger.drawJoints();

      // ------------------------------------------------
      // Selection

      switch(finger.getType()) {
        case 0:
          // System.out.println("thumb");
          break;
        case 1:
          // System.out.println("index");
          break;
        case 2:
          // System.out.println("middle");
          break;
        case 3:
          // System.out.println("ring");
          break;
        case 4:
          // System.out.println("pinky");
          break;
      }


      // ================================================
      // 5. Bones
      // --------
      // https://developer.leapmotion.com/documentation/java/devguide/Leap_Overview.html#Layer_1

      Bone    boneDistal       = finger.getDistalBone();
      // or                      finger.get("distal");
      // or                      finger.getBone(0);

      Bone    boneIntermediate = finger.getIntermediateBone();
      // or                      finger.get("intermediate");
      // or                      finger.getBone(1);

      Bone    boneProximal     = finger.getProximalBone();
      // or                      finger.get("proximal");
      // or                      finger.getBone(2);

      Bone    boneMetacarpal   = finger.getMetacarpalBone();
      // or                      finger.get("metacarpal");
      // or                      finger.getBone(3);

      // ------------------------------------------------
      // Touch emulation

      int     touchZone        = finger.getTouchZone();
      float   touchDistance    = finger.getTouchDistance();

      switch(touchZone) {
        case -1: // None
          break;
        case 0: // Hovering
          println("Hovering (#" + fingerId + "): " + touchDistance);
          break;
        case 1: // Touching
          println("Touching (#" + fingerId + ")");
          break;
      }
      
      if(gesture.type() == Gesture.Type.TYPE_CIRCLE) {
        println("Circle (#" + fingerId + ")");
      }
    }


    // ==================================================
    // 6. Tools

    for (Tool tool : hand.getTools()) {
      int     toolId           = tool.getId();
      PVector toolPosition     = tool.getPosition();
      PVector toolStabilized   = tool.getStabilizedPosition();
      PVector toolVelocity     = tool.getVelocity();
      PVector toolDirection    = tool.getDirection();
      float   toolTime         = tool.getTimeVisible();

      // ------------------------------------------------
      // Drawing:
      tool.draw();

      // ------------------------------------------------
      // Touch emulation

      int     touchZone        = tool.getTouchZone();
      float   touchDistance    = tool.getTouchDistance();

      switch(touchZone) {
        case -1: // None
          break;
        case 0: // Hovering
          println("Hovering (#" + toolId + "): " + touchDistance);
          break;
        case 1: // Touching
          println("Touching (#" + toolId + ")");
          break;
      }
    }
  }
}

void myoOnPose(de.voidplus.myo.Device myo, long timestamp, Pose pose) {
  
  if (!pose.getType().toString().equals("REST")) {
    resetImages();
  }
  
  switch (pose.getType()) {
  case REST:
    resetImages();
    break;
  case FIST:
    active[4] = true;
    // myo.vibrate();
    break;
  case FINGERS_SPREAD:
    active[1] = true;
    break;
  case DOUBLE_TAP:
    active[0] = true;
    break;
  case WAVE_IN:
    active[2] = true;
    break;
  case WAVE_OUT:
    active[3] = true;
    break;
  default:
    break;
  }
}

void myoOnLock(de.voidplus.myo.Device myo, long timestamp) {
  resetImages();
}

void myoOnUnLock(de.voidplus.myo.Device myo, long timestamp) {
  resetImages();
}
