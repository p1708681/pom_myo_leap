import numpy as np


def get_data_set(size=3):
    tmpX = None
    tmpY = None

    # Actual raw data form csv like : { [64 values separated with semicolon],class }
    tmpFile = np.genfromtxt('./data/gatheredDatas.csv', dtype=str, delimiter=",")

    # build the X datas array
    tmpX= np.genfromtxt(tmpFile[:,0], dtype=int, delimiter=";")
    
    # now build Y datas array
    tmp = tmpFile[:,1]
    tmpSize = tmp.size
    # we need to convert a number into an array like this : 
    # 0 == [1 0 0 0 0 0] OR 3 == [0 0 0 1 0 0]
    # and so on ...
    # so we create a ndarray with a size of tmpSize :: [[0 0 0 0 0 0] [0 0 0 0 0 0] [0 0 0 0 0 0] ....  [0 0 0 0 0 0]  [0 0 0 0 0 0]]
    tmpY = np.ndarray(shape=(tmpSize, size), dtype=int)

    # output an array like that [0 1 0 0 0 0]
    # it place the '1' in the created array at the pos : tmp[i] (class of the movement)
    for i in range(tmpSize):
        tmpY[i] = (np.zeros(size, dtype=int))
        index = int(tmp[i])
        tmpY[i][index] = 1
    
    #### old code ####
    # npzfile = np.load("./data/train_set.npz")
    # x = npzfile['x']
    # y = npzfile['y']

    return tmpX, tmpY
