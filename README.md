# Myo leap project

## Get started

What you will need :  
* Processing (optionnal : for the training part) ([Link here](https://processing.org/download/))
* Myo connect app ([Link here](https://support.getmyo.com/hc/en-us/articles/360018409792))
* Python ^3.6.8 (**exactly**)
* Pip ^19.0.3
* Numpy ^1.12.0
* Tensorflow ^1.1.0
* sklearn ^0.18.1
* myo-python (lastest version)

Now, launch the app `Myo connect`, perform the gesture sync.  
Go into the folder : `myo-nn_3_clusters_males/`  
launch the command : `$ python3 predict.py`  
Enjoy.  


## Gather datas and train your AI

Go into the folder : `myo-collect-EMG-datas`  
Open `Myo_3_EMG_Data.pde` (Processing needed).  

Wear the myo armband with the app launched then run the app. It will show a canvas with 8 charts, it represents the 8 electromagnetics sensors.  
Now all you have to do is to choose a pose, do it then hold a number on your key pad. Start at "0" for the first pose, then "1" for the second, and so on.
I recommend you to relax your arm then press "0" to gather datas for this pose. It will be important for the neural network and you to have this "default pose".  

Then you just have to close the little window. The datas will be writen in your default Processing folder under the name : "gatheredDatas.csv".

## Remark

Right now the AI is trained with tree poses :
* spoke : 2
* pointing : 1
* rest : 0