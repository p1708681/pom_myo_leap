import de.voidplus.myo.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.*;

Myo myo;
ArrayList<ArrayList<Integer>> sensors;
ArrayList<ArrayList<ArrayList<Integer>>> fullDatas;
ArrayList<Integer> tempArray;
int currentKey;

void setup() {
  size(800, 400);
  background(255);
  noFill();
  stroke(0);
  // ...

  myo = new Myo(this, true); // true, with EMG data
  currentKey = -1;
  
  tempArray = new ArrayList<Integer>();
  sensors = new ArrayList<ArrayList<Integer>>();
  fullDatas = new ArrayList<ArrayList<ArrayList<Integer>>>();
  for (int i=0; i<8; i++) {
    sensors.add(new ArrayList<Integer>());
    fullDatas.add(new ArrayList<ArrayList<Integer>>());
  }
  fullDatas.add(new ArrayList<ArrayList<Integer>>());
  fullDatas.add(new ArrayList<ArrayList<Integer>>());
  println("Ready !");
}

void draw() {
  background(255);
  // ...
  
  // Drawing:
  synchronized (this) {
    for (int i=0; i<8; i++) {
      if (!sensors.get(i).isEmpty()) {
        beginShape();
        for (int j=0; j<sensors.get(i).size(); j++) {
          vertex(j, sensors.get(i).get(j)+(i*50));
        }
        endShape();
      } 
    }
  }
}

// ----------------------------------------------------------

void myoOnEmgData(Device myo, long timestamp, int[] data) {
  // int[] data <- 8 values from -128 to 127
  
  synchronized (this) {
    tempArray = new ArrayList<Integer>();
    for (int i = 0; i<data.length; i++) {
      sensors.get(i).add((int) map(data[i], -128, 127, 0, 50)); // [-128 - 127]
      tempArray.add((int) map(data[i], -128, 127, -128, 127));
    }
    //println("DATAS : "+data[1]);
    //println("SENSORS : "+sensors.get(0).get(0));
    while (sensors.get(0).size() > width) {
      for(ArrayList<Integer> sensor : sensors) {
        sensor.remove(0);
      }
    }
    if (keyPressed == true) {
      writeFullDatas(tempArray);
    }
  }
  // Data:
}

void exit() {
  synchronized (this) {
    for(int i = 0; i < 10; i++) {
      writeDatas(i);
    }
    super.exit();
    println("WRITING FINISHED");
  }
}
//void keyReleased() {
//  boolForKeyPressed = true;
//}

void writeFullDatas(ArrayList<Integer> tab) {
  int tempKey = (int) key - 48;
  if(tempKey == 1 || tempKey == 2 || tempKey == 3 || tempKey == 4 || tempKey == 5 ||
     tempKey == 6 || tempKey == 7 || tempKey == 8 || tempKey == 9 || tempKey == 0) {
       println("LISTENING ON : "+tempKey);
       fullDatas.get(tempKey).add(tab);
  } else if (key == ENTER) {
      println("Stopped listening");
  }
}


void writeDatas(int i) {
  synchronized (this) {
    BufferedWriter output = null;
    PrintWriter pw = null;
    try {
      ArrayList<ArrayList<Integer>> tmpStrDatas = fullDatas.get(i);
      if(tmpStrDatas.isEmpty()) { return; }
      ArrayList<Integer> dataArray;
      File file = new File("datas/gatheredDatas.csv");
      if(!file.exists()){
        file.createNewFile();
      }
      output = new BufferedWriter(new FileWriter(file, true)); //the true will append the new data
      pw = new PrintWriter(output);
      String tmpString = "";
      for(int j = 1; j < tmpStrDatas.size(); j++) {
        dataArray = tmpStrDatas.get(j);
        for(int k = 0; k < dataArray.size(); k++) {
          tmpString = tmpString+byte(dataArray.get(k))+";";
          // pw.write(byte(dataArray.get(k))+";");
        }
        if(j%8 == 0) {
          tmpString = tmpString.substring(0, tmpString.length()-1)+","+i+"\n";
          pw.write(tmpString);
          tmpString = "";
        }
      }
    }
    catch (IOException e) {
      println("It Broke");
      e.printStackTrace();
    }
    finally {
      if (pw != null) {
        try {
          pw.close();
        } catch (Exception e) {
          println("Error while closing the writer");
        }
      }
    }
  }
}


//void keyPressed() {
//  if (key == ENTER) {
//    println("Stopped listening");
//    println(fullDatas);
//  } else {
//     println("Started listening on "+key);
//     //writeDatas((int) key);
//  }
//}
// ----------------------------------------------------------

/*
void myoOn(Myo.Event event, Device myo, long timestamp) {
  switch(event) {
    case EMG_DATA:
      break;
  }
}
*/
